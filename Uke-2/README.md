# Uke 2 - Moduler, arrays, lister, indekser, mm.


## Innhold
- Bruk av interne og eksterne moduler
- Numpy-arrays
- Python-lister
- Indekser og hvordan å bruke de
- Plotting av grafer med matplotlib.


## Info
- Øving
    - Øving 2 er ute. Aktiver den på samme måte. Frist neste torsdag.
    - Frister på øvinger er generelt sett torsdager klokken 16.
    - Feedback-runder må kjøres manuelt av admin, så det skjer ikke live.
    - Automatisk retting kan gjøre feil. Vi gjennomgår de som ikke består manuelt, for å sjekke om det er rettingen sin skyld.
    - Husk å jobbe med faget utenom øvingstimene også. Det er et stort fag og tar mange timer med arbeid.

- Jupyter Lab
    - JL funker nå! :D
    - De som har fått VS Code til å funke, kan bruke begge. Men sikkert ikke dumt å bruke VS Code, så serverne får mindre å gjøre.
    - Vi kan restarte, men trenger brukernavnet ditt.
    - Kort intro til Jupyter Lab. Fint å bruke til notater. Også i matten?

- GitLab
    - Forelesninger ligger ute - ofte en stund før forelesning
    - Inkludert oppgaver vi skal jobbe med. Men spoilers!
