---
marp: true
size: 4:3
paginate: true
class: invert
---

## Referansegruppe

> paul.knutson@ntnu.no

<!-- Dette er et nytt fag
Derfor er referansegruppen ekstra viktig.

Oppgaver:
- 3+ møter ila. semesteret
- Ref.gr.rapport (fylle inn mal)
- Attest :D
- Tar sikkert 3-4-5 timer ila. semesteret

Vil ha
- Helst 2-4 stk.
- Helst representativ
- Gjerne en nettstudent
-->
---

## Hjemmelekse
- Sett dere inn i Blackboard, GitLab, Jupyter Lab og Discord
- Begynn å programmere
    - Og begynn på øvingen
- Hvis du vil bruke f.eks. VS Code, installer og bruk det litt

<!--  -->
