# Markdown
Dette er en _Markdown_-fil. Det vil si at den består av vanlig-ish tekst, som lar oss endre på enkelte deler av formattet.

For eksempel, _kursiv_ tekst, __bold__ tekst, $matte$ og `programmering`.

Vi kan lage formeler.

$$ x = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a} $$

Vi kan lage kode.

```python
def f(x):
    return x**2

z = f(3)
print(z)
```

Men alt er bare tekst. Ikke kjørbar kode. Ingen innebygget kalkulator.
