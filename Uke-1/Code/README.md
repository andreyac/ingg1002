Her er koden skrevet i forelesning. Den er basert på Jupyter Notebook-filene i mappen over.

Legg merke til at `fil.md` er en Markdown-fil og at `fil.ipynb` er en Jupyter Notebook-fil. Denne støtter både Python-kode og Markdown-formattering. Resten (`-.py`) er vanlige Python-filer.
