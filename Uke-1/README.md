# Uke 1 - Intro til faget, Python, Jupyter og MD


## Innhold
- Introduksjon til faget INGG1002
- Python som programmeringsspråk
    - Variabler
    - Datatyper
    - mm.
- Jupyter og VS Code som utviklingsmiljøer
- Markdown (MD) og hvordan å bruke det til å skrive matematikk
