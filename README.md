# INGG1002 - Programmering, Numerikk og Sikkerhet


## Forelesningsplan
Forelesningsplan for programmeringsdelen av faget.

| Uke | Tema |
|---|---|
| 34 | Intro til faget, Python, Jupyter-filer, Markdown og VS Code. |
| 35 | Mer funksjoner, moduler, plotting, arrays, lister, index. |
| 36 | For-løkker, if og boolske verdier. |
| 37 | While-løkker, avanserte for-løkker, matriser og mer. |

Øvingene vil følge liknende temaer, og være automatisk rettet.


## Annet
Studenter burde
- Bli med i referansegruppe
- Bli studass til neste år
